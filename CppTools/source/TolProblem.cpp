/* TolProblem.hpp: API TOL Nomad (http://www.gerad.ca/nomad/Project/Home.html)

   Copyright (C) 2012, Bayes Decision, SL (Spain [EU])

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
 */

#include "TolProblem.hpp"

BText _MID("[TolNomad]");
int TolProblem::badFields_ = false;

/////////////////////////////////////////////////////////////////////////////
static NOMAD::Display& nomad_default_display(void)
/////////////////////////////////////////////////////////////////////////////
{
  //Disables NOMAD display
  //TOL can be called from graphical interfaces or remote web machines and
  //standard output is not available
  std::ostream cnull(0);
  static NOMAD::Display out ( cnull );
  out.set_degrees(NOMAD::NO_DISPLAY);
  return(out);
}

/////////////////////////////////////////////////////////////////////////////
//This class only call to TolProblem eval_x
class TolEvaluator : public NOMAD::Evaluator {
/////////////////////////////////////////////////////////////////////////////
public:
  const TolProblem & p_;
  TolEvaluator  ( const TolProblem & p )
  : NOMAD::Evaluator ( p.nomad_ ),  p_(p) {};

 ~TolEvaluator ( void ) {};

  bool eval_x ( NOMAD::Eval_Point& x,
    const NOMAD::Double & h_max,
    bool                & count_eval   ) const
  {
    return(p_.eval_x(x,h_max,count_eval));
  }
};


/////////////////////////////////////////////////////////////////////////////
//C++ TolProblem class instance handler to be stored in TolNomad::@Problem
//user instance as a double number (BDat)
union TolProblemAddress
/////////////////////////////////////////////////////////////////////////////
{
  TolProblem *ptr_;
  double      hnd_; 
};

/////////////////////////////////////////////////////////////////////////////
BDat TolProblem::code_addr( TolProblem& ptr )
/////////////////////////////////////////////////////////////////////////////
{
  TolProblemAddress convert;
  convert.ptr_ = &ptr;
  return convert.hnd_;
}

/////////////////////////////////////////////////////////////////////////////
TolProblem& TolProblem::decode_addr( BDat& hnd )
/////////////////////////////////////////////////////////////////////////////
{
  TolProblemAddress convert;
  convert.hnd_ = hnd.Value();
  return *(convert.ptr_);
}


/////////////////////////////////////////////////////////////////////////////
bool TolProblem::check_is_finite(BDat& v, const BText& name)
/////////////////////////////////////////////////////////////////////////////
{
  int nf = !(v.IsFinite());
  if(nf==0) { return (true); }
  else
  {
    Error(_MID+" Member Real "+name+"="+v+" is not a known finite value\n");
    badFields_ = true;
    return(false);
  }
}

/////////////////////////////////////////////////////////////////////////////
bool TolProblem::check_is_finite(BVMat& v, const BText& name)
/////////////////////////////////////////////////////////////////////////////
{
  int nf = (int)v.IsFinite().Not().Sum();
  if(nf==0) { return (true); }
  else
  {
    Error(_MID+" Member VMatrix "+name+" has "+nf+" unknown or infinite values\n");
    badFields_ = true;
    return(false);
  }
}

/////////////////////////////////////////////////////////////////////////////
bool TolProblem::check_is_finite(BMat& v, const BText& name)
/////////////////////////////////////////////////////////////////////////////
{
  BVMat V; V.DMat2dense((BMatrix<double>&)v);
  int nf = (int)V.IsFinite().Not().Sum();
  if(nf==0) { return (true); }
  else
  {
    Error(_MID+" Member Matrix "+name+" has "+nf+" unknown or infinite values\n");
    badFields_ = true;
    return(false);
  }
}

/////////////////////////////////////////////////////////////////////////////
BNameBlock& TolProblem::EnsureWrapper(BNameBlock& wrapper)
/////////////////////////////////////////////////////////////////////////////
{
//Std(BText("\nTRACE TolProblem::EnsureMember(")<<name<<") 1");
  if(!wrapper.IsInstanceOf("TolNomad::@Problem"))
  {
    Error(_MID+"An instance of TolNomad::@Problem was expected "
     "instead of "<<wrapper.Name());
    badFields_ = true;
    BGrammar::Turn_StopFlag_On(); 
  }
  return(wrapper);
};

/////////////////////////////////////////////////////////////////////////////
BSyntaxObject* TolProblem::EnsureMember(const BText& name)
/////////////////////////////////////////////////////////////////////////////
{
//Std(BText("\nTRACE TolProblem::EnsureMember(")<<name<<") 1");
  BSyntaxObject* mbm = wrapper_.Member(name);
//Std(BText("\nTRACE TolProblem::EnsureMember(")<<name<<") 2");
  if(!mbm)
  {
    Error(_MID+"Member name "+name+" is not found");
    badFields_ = true;
    BGrammar::Turn_StopFlag_On(); 
  };
  return(mbm);
};



/////////////////////////////////////////////////////////////////////////////
TolProblem::TolProblem(BNameBlock& wrapper)
/////////////////////////////////////////////////////////////////////////////
:
  nomad_(nomad_default_display()),
  isGood_(true),
  isInitialized(true),
  mads_(NULL),
  wrapper_(EnsureWrapper(wrapper)),
  n_(&Dat(EnsureMember("n"))),n(-1),
  r_(&Dat(EnsureMember("r"))),r(-1),
  sign_(&Dat(EnsureMember("sign"))),
  x_type(&Set(EnsureMember("x_type"))),
  x_lower(&Mat(EnsureMember("x_lower"))),
  x_upper(&Mat(EnsureMember("x_upper"))),
  x_init(&Mat(EnsureMember("x_init"))),
  max_eval(&Dat(EnsureMember("max_eval"))),
  max_time(&Dat(EnsureMember("max_time"))),
  min_mesh_size_rel(&Dat(EnsureMember("min_mesh_size_rel"))),
  min_poll_size_rel(&Dat(EnsureMember("min_poll_size_rel"))),
  trace_time(&Dat(EnsureMember("trace_time"))),
  cache_file(&Text(EnsureMember("cache_file"))),
  cache_save_period(&Dat(EnsureMember("cache_save_period"))),
  cache_memory(&Dat(EnsureMember("cache_memory"))),
  x_opt_constrained(&Mat(EnsureMember("_.x_opt_constrained"))),
  f_opt_constrained(&Dat(EnsureMember("_.f_opt_constrained"))),
  g_opt_constrained(&Mat(EnsureMember("_.g_opt_constrained"))),
  x_opt_unconstrained(&Mat(EnsureMember("_.x_opt_unconstrained"))),
  f_opt_unconstrained(&Dat(EnsureMember("_.f_opt_unconstrained"))),
  g_opt_unconstrained(&Mat(EnsureMember("_.g_opt_unconstrained"))),
  stop_desc(&Text(EnsureMember("_.stop_desc"))),
  stats_report(&Set(EnsureMember("_.stats_report"))),
  _handler(&Dat(EnsureMember("_.handler")))
{
  //TOL capsule of evaluation argument is created just one time and
  //is protected from deletion
  uX  = new BContensMat;
  uX->IncNRefs();
  uX->IncNRefs();

  BSyntaxObject* target_member = wrapper_.PublicMember("nomad_target"); 
  if(!target_member)
  {
    Error(_MID+"Method nomad_target is not defined.");
    badFields_ = true;
  }
  else
  {
    nomad_target = &Code(target_member);
  }
  if(badFields_) 
  { 
    badFields_ = false;
    isGood_ = false;
    Error(_MID+" Error during creation of TolProblem!\n");
    return; 
  }
} 

/////////////////////////////////////////////////////////////////////////////
/** Default destructor */
TolProblem::~TolProblem()
/////////////////////////////////////////////////////////////////////////////
{
  //Unprotect and delete TOL capsule of evaluation argument
  uX->DecNRefs();
  uX->DecNRefs();
  DESTROY(uX);
}


/////////////////////////////////////////////////////////////////////////////
static bool wrong_type_def(int n)
/////////////////////////////////////////////////////////////////////////////
{
  Error(_MID+"Member Set x_type must have "+n+" Text elements, representing "
        "the numeric type of each variable, and must start at one of these "
          "characters: \n"+
          "  r : for continuous real numbers\n"+ 
          "  i : for integer numbers\n"+ 
          "  c : for categorical values\n"+ 
          "  b : for binary values\n");
  return(false);
}


/////////////////////////////////////////////////////////////////////////////
bool TolProblem::initialize()
/////////////////////////////////////////////////////////////////////////////
{
  BDat y;
  if(!isGood_) { return(false); }
  if(n_->IsUnknown() || !n_->IsFinite() || Round(*n_)!=*n_ || *n_<=0)
  {
    Error(_MID+"Dimension problem {n="+*n_+"} is not a positive integer.");
    return(isGood_ = false);
  }
  if(r_->IsUnknown() || !r_->IsFinite() || Round(*r_)!=*r_ || *r_<0)
  {
    Error(_MID+"Restrictions number {r="+*r_+"} is not a non negative integer.");
    return(isGood_ = false);
  }
  n = (int)n_->Value();
  r = (int)r_->Value();

  BMat& X = uX->Contens();
  X.Alloc(n, 1);
  BMatrix<double>& xBF = *(BMatrix<double>*)(x_opt_constrained);
  BMatrix<double>& gBF = *(BMatrix<double>*)(g_opt_constrained);
  xBF.Alloc(n,1); xBF.SetAllValuesTo(BDat::Unknown().Value());
  gBF.Alloc(r,1); gBF.SetAllValuesTo(BDat::Unknown().Value());
  *f_opt_constrained = BDat::Unknown();
  BMatrix<double>& xUF = *(BMatrix<double>*)(x_opt_unconstrained);
  BMatrix<double>& gUF = *(BMatrix<double>*)(g_opt_unconstrained);
  xUF.Alloc(n,1); xUF.SetAllValuesTo(BDat::Unknown().Value());
  gUF.Alloc(r,1); gUF.SetAllValuesTo(BDat::Unknown().Value());
  *f_opt_unconstrained = BDat::Unknown();

  if(x_type->Card()!=n) { return(isGood_ = wrong_type_def(n)); }
  if(x_init->Rows()!=n || x_init->Columns()!=1)
  {
    Error(_MID+"Member Matrix x_init must be a ("+n+"x1) column matrix");
    isGood_ = false;
  }
  if(x_lower->Rows()!=n || x_lower->Columns()!=1)
  {
    Error(_MID+"Member Matrix x_lower must be a ("+n+"x1) column matrix");
    isGood_ = false;
  }
  if(x_upper->Rows()!=n || x_upper->Columns()!=1)
  {
    Error(_MID+"Member Matrix x_upper must be a ("+n+"x1) column matrix");
    isGood_ = false;
  }
  if(!isGood_) { return(false); }
  try 
  { 
    int i;
    nomad_.set_DIMENSION (n); // number of variables
    // definition of output types:
    vector<NOMAD::bb_output_type> bbot (r+1); 
    bbot[0] = NOMAD::OBJ;                   
    for(i=1; i<=r; i++) { bbot[i] = NOMAD::PB; }
    nomad_.set_BB_OUTPUT_TYPE ( bbot );
    for(i=0; i<n; i++) 
    { 
      if((*x_type)[i+1]->Grammar()!=GraText()) { return(isGood_ = wrong_type_def(n)); }
      const BText& type = Text((*x_type)[i+1]);
      char t = tolower(type[0]);
           if(t=='r') { nomad_.set_BB_INPUT_TYPE(i,NOMAD::CONTINUOUS); }
      else if(t=='i') { nomad_.set_BB_INPUT_TYPE(i,NOMAD::INTEGER); }
      else if(t=='c') { nomad_.set_BB_INPUT_TYPE(i,NOMAD::CATEGORICAL); }
      else if(t=='b') { nomad_.set_BB_INPUT_TYPE(i,NOMAD::BINARY); }
      else { return(isGood_ = wrong_type_def(n)); }
    }

    NOMAD::Point x0 (n);
    for(i=0; i<n; i++) { x0[i] = (*x_init)(i,0).Value(); }
    nomad_.set_X0 (x0);
    bool lb_free = true;
    NOMAD::Point lb (n);
    for(i=0; i<n; i++) 
    { 
      y = (*x_lower)(i,0);
      if(!y.IsUnknown() && y!=BDat::NegInf()) { lb_free = false; }
      lb[i] = y.Value(); 
    }
    if(!lb_free) { nomad_.set_LOWER_BOUND ( lb ); }

    bool ub_free = true;
    NOMAD::Point ub (n);
    for(i=0; i<n; i++) 
    { 
      y = (*x_upper)(i,0);
      if(!y.IsUnknown() && y!=BDat::PosInf()) { ub_free = false; }
      ub[i] = y.Value(); 
    }
    if(!ub_free) { nomad_.set_UPPER_BOUND ( ub ); }

    if(!max_eval->IsUnknown() && max_eval->IsFinite() && *max_eval>0)
    {
      nomad_.set_MAX_BB_EVAL (int(max_eval->Value()));
    }
    if(!max_time->IsUnknown() && max_time->IsFinite() && *max_time>0)
    {
      nomad_.set_MAX_TIME (max_time->Value());
    }
    if(!min_mesh_size_rel->IsUnknown() && min_mesh_size_rel->IsFinite() && *min_mesh_size_rel>0)
    {
      NOMAD::Double aux = min_mesh_size_rel->Value();
      nomad_.set_MIN_MESH_SIZE(aux,true);
    }
    if(!min_poll_size_rel->IsUnknown() && min_poll_size_rel->IsFinite() && *min_poll_size_rel>0)
    {
      NOMAD::Double aux = min_poll_size_rel->Value();
      nomad_.set_MIN_POLL_SIZE(aux,true);
    }
    if(cache_file->Length())
    {
      nomad_.set_CACHE_FILE (cache_file->String());
    }
    if(!cache_memory->IsUnknown() && *cache_memory>0)
    {
      nomad_.set_MAX_CACHE_MEMORY ((float)cache_memory->Value());
    }
    if(!cache_save_period->IsUnknown() && *cache_save_period>0)
    {
      nomad_.set_CACHE_SAVE_PERIOD ((int)cache_save_period->Value());
    }
    nomad_.check();
  } 
  catch ( exception & e ) 
  {
    isGood_ = false;
    Error(_MID+" NOMAD problem is wrong defined: \n " + e.what() + "\n");
    isInitialized = false;
    return(false);
  }
  isInitialized=true;
  return(true);
};

/////////////////////////////////////////////////////////////////////////////
void TolProblem::set_best_constrained(const NOMAD::Eval_Point& bf)
/////////////////////////////////////////////////////////////////////////////
{
  int i;
  const NOMAD::Point bfo = bf.get_bb_outputs();
  BDat f = -*sign_*bfo[0].value();
  *f_opt_constrained = f;
  BMatrix<double>& xBF = *(BMatrix<double>*)(x_opt_constrained);
  BMatrix<double>& gBF = *(BMatrix<double>*)(g_opt_constrained);
  for(i=0; i<n; i++) { xBF(i,0)=bf[i].value(); }
  for(i=1; i<=r; i++) 
  { 
    gBF(i-1,0) = bfo[i].value();
  }
}

/////////////////////////////////////////////////////////////////////////////
void TolProblem::set_best_unconstrained(const NOMAD::Eval_Point& uf)
/////////////////////////////////////////////////////////////////////////////
{
  int i;
  const NOMAD::Point ufo = uf.get_bb_outputs();
  BDat f = -*sign_*ufo[0].value();
  *f_opt_unconstrained = f;
  BMatrix<double>& xUF = *(BMatrix<double>*)(x_opt_unconstrained);
  BMatrix<double>& gUF = *(BMatrix<double>*)(g_opt_unconstrained);
  for(i=0; i<n; i++) { xUF(i,0)=uf[i].value(); }
  for(i=1; i<=r; i++) 
  { 
    gUF(i-1,0) = ufo[i].value();
  }
}

/////////////////////////////////////////////////////////////////////////////
void TolProblem::check_best(const NOMAD::Eval_Point& point)
/////////////////////////////////////////////////////////////////////////////
{
  const NOMAD::Point output = point.get_bb_outputs();
  BDat f = -*sign_*output[0].value();
  if(f_opt_unconstrained->IsUnknown() || (f< *f_opt_unconstrained))
  {
    set_best_unconstrained(point);
  }
  if(point.is_feasible(nomad_.get_h_min()))
  {
    if(f_opt_constrained->IsUnknown() || (f< *f_opt_constrained))
    {
      set_best_constrained(point);
    }
  }
}

/////////////////////////////////////////////////////////////////////////////
void TolProblem::get_mads_best(void)
/////////////////////////////////////////////////////////////////////////////
{
  if(mads_)
  {
    const NOMAD::Eval_Point * bf = mads_->get_best_feasible();
    if(bf)
    {
      set_best_constrained(*bf);
      set_best_unconstrained(*bf);
    }
    const NOMAD::Eval_Point * uf = mads_->get_best_infeasible();
    if(uf)
    {
      set_best_unconstrained(*uf);
    }
  }
}

/////////////////////////////////////////////////////////////////////////////
bool TolProblem::eval_x ( NOMAD::Eval_Point& x,
    const NOMAD::Double & h_max,
    bool                & count_eval   ) const
/////////////////////////////////////////////////////////////////////////////
{
  TolProblem* T = (TolProblem*)this;
  T->numEval++;
  int i;
  BDat y;
  //Activates the user TOL method to be evaluated
  BCode& code = *(BCode*)nomad_target;
  code.Operator()->PutNameBlock(&wrapper_);
  //Creates the list of arguments
  BList* lst = Cons(uX,NULL);
  //Fills the argument values
  BMat& X = uX->Contens();
  for(i=0; i<n; i++) { X(i,0)=x[i].value(); }
  //Evaluates de user TOL method
  BUserMat* uY  = UMat(code.Evaluator(lst)); 
  //Takes the returned values
  BMat& Y = uY->Contens();
  //checks returned dimensions
  count_eval = Y.Rows()==r+1 && Y.Columns()==1;
  if(count_eval)
  {
    for(i=0; i<=r; i++) 
    { 
      y = Y(i,0);
      //Sign of optimization: -1 for minimization, +1 for maximization
      if(i==0) { y*=-sign_->Value(); }
      if(y.IsUnknown()) { count_eval = false; }
      x.set_bb_output(i,y.Value()); 
    }
  }
  //Destroys returned TOL object and the list of arguments
  DESTROY(uY);
  //Show traces every specified lapse of time
  double clk = double(BTimer::ClockToSecond(timer.Clk()));
  if(clk-lastMsgClock>trace_time->Value())
  {
    T->lastMsgClock = clk;
    T->get_mads_best();
    int currentNumObj = BSyntaxObject::NSyntaxObject();
    Std(_MID+" Evaluation number:"+numEval+
        ", best unconstrained:"+(*f_opt_unconstrained)+
        ", best constrained:"+(*f_opt_constrained)+
        ", number of TOL objects:"+currentNumObj+
        ", elapsed CPU:"+clk+"seconds\n");
  }
  return(count_eval);
}



/////////////////////////////////////////////////////////////////////////////
bool TolProblem::optimize()
/////////////////////////////////////////////////////////////////////////////
{
  if(!isGood_) { return(false); }
  try 
  { 
    if(!isInitialized) { initialize(); }
    nomad_.set_DISPLAY_DEGREE(NOMAD::NO_DISPLAY,NOMAD::NO_DISPLAY,NOMAD::NO_DISPLAY,NOMAD::NO_DISPLAY);
    nomad_.set_DISPLAY_STATS("");
    // custom evaluator creation:
    TolEvaluator ev( *this );
    // algorithm creation and execution:
    NOMAD::stop_type st;
    double elapsedTime, cpuClock;
    NOMAD::Mads mads ( nomad_, &ev );
    mads_ = &mads;
    numEval = 0;
    numObj = BSyntaxObject::NSyntaxObject();
    timer.Reset();
    lastMsgClock = double(BTimer::ClockToSecond(timer.Clk()));
    st = mads.run();
    elapsedTime = double(timer.MSec())/1000;
    cpuClock = double(BTimer::ClockToSecond(timer.Clk()));
    switch(st) {
      case NOMAD::NO_STOP : *stop_desc="No stop."; break;
      case NOMAD::ERROR : *stop_desc="Error."; break;
      case NOMAD::UNKNOWN_STOP_REASON : *stop_desc="Unknown."; break;
      case NOMAD::CTRL_C : *stop_desc="Ctrl-C."; break;
      case NOMAD::USER_STOPPED : *stop_desc="User-stopped in Evaluator::update_iteration()"; break;
      case NOMAD::MESH_PREC_REACHED : *stop_desc="Mesh minimum precision reached."; break;
      case NOMAD::X0_FAIL : *stop_desc="Problem with starting point evaluation."; break;
      case NOMAD::P1_FAIL : *stop_desc="Problem with phase one."; break;
      case NOMAD::DELTA_M_MIN_REACHED : *stop_desc="Min mesh size."; break;
      case NOMAD::DELTA_P_MIN_REACHED : *stop_desc="Min poll size."; break;
      case NOMAD::L_MAX_REACHED : *stop_desc="Max mesh index."; break;
      case NOMAD::L_LIMITS_REACHED : *stop_desc="Mesh index limits."; break;
      case NOMAD::MAX_TIME_REACHED : *stop_desc="Max time."; break;
      case NOMAD::MAX_BB_EVAL_REACHED : *stop_desc="Max number of blackbox evaluations."; break;
      case NOMAD::MAX_SGTE_EVAL_REACHED : *stop_desc="Max number of surrogate evaluations."; break;
      case NOMAD::MAX_EVAL_REACHED : *stop_desc="Max number of evaluations."; break;
      case NOMAD::MAX_SIM_BB_EVAL_REACHED : *stop_desc="Max number of sim bb evaluations."; break;
      case NOMAD::MAX_ITER_REACHED : *stop_desc="Max number of iterations."; break;
      case NOMAD::MAX_CONS_FAILED_ITER : *stop_desc="Max number of consecutive failed iterations."; break;
      case NOMAD::FEAS_REACHED : *stop_desc="Feasibility."; break;
      case NOMAD::F_TARGET_REACHED : *stop_desc="F_TARGET."; break;
      case NOMAD::STAT_SUM_TARGET_REACHED : *stop_desc="STAT_SUM_TARGET."; break;
      case NOMAD::L_CURVE_TARGET_REACHED : *stop_desc="L_CURVE_TARGET."; break;
      case NOMAD::MULTI_MAX_BB_REACHED : *stop_desc="Max number of blackbox evaluations (multi obj.)"; break;
      case NOMAD::MULTI_NB_MADS_RUNS_REACHED : *stop_desc="Max number of MADS runs (multi obj.)"; break;
      case NOMAD::MULTI_STAGNATION : *stop_desc="Stagnation criterion (multi obj.)"; break;
      case NOMAD::MULTI_NO_PARETO_PTS : *stop_desc="No Pareto points (multi obj.)"; break;
      case NOMAD::MAX_CACHE_MEMORY_REACHED : *stop_desc="Max cache memory."; break;
      default : *stop_desc="Unhandled reason."; 
    };
    BGrammar::IncLevel();
    NOMAD::Stats& stats = mads.get_stats();
    stats_report->Delete();
    stats_report->AddElement(new BContensDat("elapsedTime",elapsedTime,"Elapsed time in seconds."));
    stats_report->AddElement(new BContensDat("cpuClock",elapsedTime,"Used CPU in seconds."));
    stats_report->AddElement(new BContensDat("eval",stats.get_eval(),"Number of evaluations."));
    stats_report->AddElement(new BContensDat("sim_bb_eval",stats.get_sim_bb_eval(),"Number of simulated blackbox evaluations."));
    stats_report->AddElement(new BContensDat("sgte_eval",stats.get_sgte_eval(),"Number of surrogate evaluations."));
    stats_report->AddElement(new BContensDat("bb_eval",stats.get_bb_eval(),"Number of blackbox evaluations."));
    stats_report->AddElement(new BContensDat("failed_eval",stats.get_failed_eval(),"Number of failed evaluations."));
    stats_report->AddElement(new BContensDat("cache_hits",stats.get_cache_hits(),"Number of cache hits."));
    stats_report->AddElement(new BContensDat("iterations",stats.get_iterations(),"Number of iterations."));
  //stats_report->AddElement(new BContensDat("stat_sum",stats.get_stat_sum().value(),"Sum stat (output of type NOMAD::STAT_SUM)."));
  //stats_report->AddElement(new BContensDat("stat_avg",stats.get_stat_avg().value(),"Average stat (output of type NOMAD::STAT_AVG)."));
    stats_report->AddElement(new BContensDat("mads_runs",stats.get_mads_runs(),"Number of MADS runs (for multi-objective runs)."));
    stats_report->AddElement(new BContensDat("LH_pts",stats.get_LH_pts(),"Number of Latin-Hypercube (LH) search points."));
    stats_report->AddElement(new BContensDat("CS_pts",stats.get_CS_pts(),"Number of CS search points."));
    stats_report->AddElement(new BContensDat("VNS_bb_eval",stats.get_VNS_bb_eval(),"Number of VNS blackbox evaluations."));
    stats_report->AddElement(new BContensDat("VNS_sgte_eval",stats.get_VNS_sgte_eval(),"Number of VNS surrogate evaluations."));
    BGrammar::DecLevel();
    get_mads_best();
    mads_ = NULL;
  } 
  catch ( exception & e ) 
  {
    isGood_ = false;
    Error(_MID+" NOMAD optimization has been interrupted: \n " + e.what() + "\n");
  }
  return(isGood_);
}

